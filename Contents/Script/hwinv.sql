##-- loveland DYL HW INV CREATE TABLE & INSERT VALS

CREATE TABLE IF NOT EXISTS `hwinv` (
  `id` int(2),
  `Name` varchar(20),
  `Employee` varchar(1),
  `OnLease` varchar(1),
  `CompAvail` varchar(1),
  `Email` varchar(20),
  `Ext` varchar(3),
  `Cell` varchar(14),
  `ShiftIn` varchar(8),
  `ShiftOut` varchar(8),
  `HireDate` varchar(11),
  `EndDate` varchar(11),
  `Bday` varchar(11),
  `Position` varchar(11),
  `PositionPlus` varchar(40),
  `RemoteEmp` varchar(1),
  `Parking` varchar(1),
  `ParkingCard` varchar(5),
  `BldgAcc` varchar(1),
  `BldgCard` varchar(20),
  `OfficeKeyCard` varchar(6),
  `Gym` varchar(1),
  `Note` varchar (40),
  `LanPort` varchar(2),
  `Hostname` varchar(30),
  `Brand` varchar(20),
  `ProductName` varchar(10),
  `ComputerModel` varchar(20),
  `HWModel` varchar(15),
  `ComputerVersion` varchar(10),
  `BuildVersion` varchar(5),
  `KernelVersion` varchar(16),
  `InstalledRAM` varchar(2),
  `DYLUpgrade` varchar(1),
  `RAMUpgrade` varchar(2),
  `SSDUpgrade` varchar(1),
  `ProcessorName` varchar(16),
  `ProcessorSpeed` varchar(8),
  `NumOfProcessors` varchar(1),
  `Cores` varchar(1),
  `L2CachePerCore` varchar(6),
  `L3Cache` varchar(4),
  `SMCVersion` varchar(9),
  `BootRomVer` varchar(20),
  `SerialNumber` varchar(20),
  `HardwareUUID` varchar(40),
  `LANMAC` varchar(31),
  `WIFIMAC` varchar(31),
  `DeskPhoneMake` varchar(20),
  `DeskPhoneModel` varchar(20),
  `DeskPhoneMAC` varchar(20),
  `MoreDevices` varchar(1),
  `ComputerModelPlus` varchar(20),
  `SerialPlus` varchar(20),
  `LANMACPlus` varchar(31),
  `WIFIMACPlus` varchar(31),
  `HostnamePlus` varchar(20)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
